import 'package:flutter/material.dart';
import './cat.dart';
import 'cat_dao.dart';

void main() async {
  var cat1 = Cat(
    id: 0,
    name: 'Louis',
    age: 30,
  );
  var cat2 = Cat(
    id: 1,
    name: 'Wagyu',
    age: 25,
  );
  await CatDao.insertCat(cat1);
  await CatDao.insertCat(cat2);

  print(await CatDao.cats());
 

  
  cat1 = Cat(
    id: cat1.id,
    name: 'Pandora',
    age: cat1.age+6,
  );
  await CatDao.updateCat(cat1);
  print(await CatDao.cats());


  await CatDao.deleteCat(0);
  print(await CatDao.cats());
}


